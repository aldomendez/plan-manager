# Plan manager

Simple text based plan manager application

This is based on the idea [tability](https://tability.app/proyectos/inbox) but based
on the premise that we should be able to move out conten where ever we want in a
portable and readable format, the idea is to make a program that can read those files
and can show them to be consumed or modified directly in a better that yust the file
way.

It should be as easy as markdown but with the semantics of a planner with objectives, outcomes, tasks, checkins, comments, team members and all that stuff. If we just read the file with a markdown
editor it should make sense in format, titles, comments organization and so on.


# sources of inspiration:
Parser
https://lisperator.net/pltut/
https://github.com/harc/ohm/blob/master/doc/syntax-reference.md
https://lihautan.com/json-parser-with-javascript/
https://www.crockford.com/mckeeman.html
https://github.com/sveltejs/svelte/blob/master/src/compiler/parse/index.ts
https://ohmlang.github.io/editor/#30325d346a6e803cc35344ca218d8636
https://github.com/harc/ohm
https://lisperator.net/
https://accu.org/journals/overload/26/145/balaam_2510/
https://accu.org/journals/overload/26/146/balaam_2532/
https://accu.org/journals/overload/26/147/balaam_2565/
https://adriann.github.io/rust_parser.html
https://tiarkrompf.github.io/notes/?/just-write-the-parser/aside6
https://stlab.cc/legacy/how-to-write-a-simple-lexical-analyzer-or-parser.html
https://microsoft.github.io/monaco-editor/monarch.html
https://github.com/todotxt/todo.txt
https://www.markdownguide.org/cheat-sheet
https://tability.app/proyectos/inbox
https://lark-parser.readthedocs.io/en/latest/examples/indented_tree.html