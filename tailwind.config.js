const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

module.exports = {
  darkMode: 'media',
  mode: 'jit',
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    // classes that are generated dynamically, e.g. `rounded-${size}` and must
    // be kept
    safeList: [],
    content: [
      './index.html',
      './src/**/*.vue',
      './src/**/*.js',
      // etc.
    ],
  },
  theme: {
    colors,
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
    require('daisyui'),
  ],
  daisyui: {
    themes: [
      {
        'novalink': {                          /* your theme name */
          'primary': '#2762EA',           /* Primary color */
          'primary-focus': '#2256CC',     /* Primary color - focused */
          'primary-content': '#F7F6F6',   /* Foreground content color to use on primary color */

          'secondary': '#7B92B2',         /* Secondary color */
          'secondary-focus': '#5C789D',   /* Secondary color - focused */
          'secondary-content': '#F7F6F6', /* Foreground content color to use on secondary color */

          'accent': '#37cdbe',            /* Accent color */
          'accent-focus': '#2aa79b',      /* Accent color - focused */
          'accent-content': '#F7F6F6',    /* Foreground content color to use on accent color */

          'neutral': '#3d4451',           /* Neutral color */
          'neutral-focus': '#2a2e37',     /* Neutral color - focused */
          'neutral-content': '#ffffff',   /* Foreground content color to use on neutral color */

          'base-100': '#ffffff',          /* Base color of page, used for blank backgrounds */
          'base-200': '#f9fafb',          /* Base color, a little darker */
          'base-300': '#d1d5db',          /* Base color, even more darker */
          'base-content': '#1f2937',      /* Foreground content color to use on base color */

          'info': '#2094f3',              /* Info */
          'success': '#009485',           /* Success */
          'warning': '#ff9900',           /* Warning */
          'error': '#ff5724',             /* Error */

          '--rounded-box': '0.2rem',        /* border-radius for cards and other big elements */
          '--rounded-btn': '0.2rem',      /* border-radius for buttons and similar elements */
          '--rounded-badge': '0.2rem',    /* border-radius for badge and other small elements */

          '--animation-btn': '0.15s',     /* bounce animation time for button */
          '--animation-input': '.2s',     /* bounce animation time for checkbox, toggle, etc */

          '--padding-card': '1rem',       /* default card-body padding */

          '--btn-text-case': 'normal-case', /* default text case for buttons */
          '--navbar-padding': '.5rem',    /* default padding for navbar */
          '--border-btn': '1px',          /* default border size for button */
          '--focus-ring': '2px',          /* focus ring size for button and inputs */
          '--focus-ring-offset': '1px',   /* focus ring offset size for button and inputs */
        },
      },
      // 'corporate',
      // 'emerald', // first one will be the default theme
      // 'dark',
      // 'forest',
      // 'synthwave',
    ],
  },
}
