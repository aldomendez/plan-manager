import NotFound from './views/NotFound.vue'

/** @type {import('vue-router').RouterOptions['routes']} */
export let routes = [
  {
    path: '/', component: () => import('./views/Shell.vue'), children: [
      { path: '', component: () => import('./views/PlanList.vue') },
      { path: 'plan/:plan', component: () => import('./views/PlanDetails.vue') },
      { path: 'plan/:plan/edit', component: () => import('./views/PlanEdit.vue') },

    ]
  },
  { path: '/:path(.*)', component: NotFound }
]
