class Stack {
    collection = []
    capabilities = {
        Plans: ['Plan'],
        Plan: ['Objective'],
        Objective: ['Outcome'],
        Outcome: ['Task', 'CheckIn', 'Comments'],
        Task: ['Comments'],
        CheckIn: ['Comments'],
        Comments: ['Comments']
    }
    push(curr) {
        if (this.collection.length === 0) {
            if (curr.type !== 'Plans') throw new Error('First element Should be a Plan')
            this.collection.push(curr)
        } else {
            let last = this.collection.length - 1
            let lastType = this.collection[last].type
            let canDiggest = this.capabilities[lastType].includes(curr.type)

            if (canDiggest) {
                this.collection.push(curr)
            } else {
                this.digest(curr.type)
                this.collection.push(curr)
            }
        }
    }
    canDiggest(lastType) {
        let s = this.capabilities[lastType].includes(lastType)
        return s
    }
    set(key, value) {
        let last = this.collection.length - 1
        this.collection[last][key] = value
    }
    digest(type) {
        do {
            let last = this.collection.pop()
            let l = this.collection.length - 1
            this.collection[l].digest(last)
        } while (this.collection.length >= 0 && this.canDiggest(type))
    }
    commitUpTo(type){
        var l = this.collection.length - 1
        while(this.collection[this.collection.length - 1].type !== type){
            let last = this.collection.pop()
            var m = this.collection.length - 1
            this.collection[m].digest(last)
            l = this.collection.length - 1
        }
    }
    commit() {
        do {
            let last = this.collection.pop()
            let lastType = last.type
            let l = this.collection.length - 1
            this.collection[l].digest(last)
        } while (this.collection.length > 1)
    }
    current(){
        return this.collection[0]
    }

}
class Plans {
    type = 'Plans'
    collection = []
    digest(p) {
        this.collection.push(p)
    }
}
class Plan {
    type = 'Plan'
    completeness = 0
    priority = ''
    begin = ''
    end = ''
    description = ''
    tags = []
    contexts = []
    users = []
    special = {}
    objectives = []
    constructor() { }
    digest(el) {
        if (el.type == 'Objective') {
            this.objectives.push(el)
        }
    }
    toString() {
        return `# Plan(${this.title})`
    }
}
class Objective {
    type = 'Objective'
    title = ''
    outcomes = []
    digest(el) {
        if (el.type == 'Outcome') {
            this.outcomes.push(el)
        }
    }
    toString() {
        return `## Objective(${this.title})`
    }
}
class Outcome {
    type = 'Outcome'
    description = ''
    owner = null
    initial = ''
    expected = ''

    progress = []
    tasks = []
    comments = []
    digest(el) {
        if (el.type == 'CheckIn') {
            this.progress.push(el)
        } else if (el.type == 'Task') {
            this.tasks.push(el)
        } else if (el.type == 'Comments') {
            this.comments.push(el)
        }
    }
    toString() {
        return `### Outcome(${this.title})`
    }
}
class Task {
    type = 'Task'
    done = false
    owner = ''
    dueAt = null
    done = null
    description = ''
    comments = []
    constructor() { }
    digest(el) {
        if (el.type == 'Comments') {
            this.comments.push(el)
        }
    }
}
class CheckIn {
    type = 'CheckIn'
    date = null
    progress = ''
    confidence = ''
    analysis = ''
    comments = []
    digest(el) {
        if (el.type == 'Comments') {
            this.comments.push(el)
        }
    }
}
class Comments {
    type = 'Comments'
    author = ''
    date = ''
    comment = ''
    comments = []
    digest(el) {
        if (el.type == 'Comments') {
            this.comments.push(el)
        }
    }
}

export {
    Stack,
    Plans,
    Plan,
    Objective,
    Outcome,
    Task,
    CheckIn,
    Comments,
}