import axios from 'axios'
let baseUrl = process.env.NODE_ENV === "production" ? "/" : "http://nova-pix.test/";

axios.defaults.baseURL = baseUrl

axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
  'Accept': 'application/json',
  'Content-Type': 'application/json',
}
// axios.defaults.withCredentials=true

const setToken = (t) => {
  t ? localStorage.setItem('access_token', t) : localStorage.removeItem('access_token')
  axios.defaults.headers.common['Authorization'] = t ? `Bearer ${t}` : null
}

setToken(localStorage.access_token)

const get = axios.get;
const post = axios.post;
const put = axios.put;
const del = axios.delete;


export { post, get, del, put, setToken };
