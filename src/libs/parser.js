const titleDetailsPattern = /(\d*)% \(([\w]*)\) ([\d|-]*) ([\d|-]*)(.*)/i
const readTitleHeader = (tt) => {
    let [header, details, ...description] = tt.split('\n')
    let [_, completeness, priority, begin, ends] = titleDetailsPattern.exec(details)
    return {
        title: header,
        completeness, priority, begin, ends,
        description
    }
}
const handleObjectives = (pzs) => {
    let os = '\n### '
    return pzs.map(obj => {
        if (obj.includes(os)) {
            let [header, ...outcomes] = obj.trim().split(os)
            return {
                type: 'objective',
                title: header,
                outcomes: handleOutcomes(outcomes)
            }
        } else {
            return {
                type: 'error',
                content: obj,
                message: "🤷‍♂️ where are the objectives? 🚨"
            }
        }
    })
}
const outcomeTitlePattern = /(@[\w]+) ([\d]*) to ([ |\d|%]*): ([\w|\W]*)/im
const handleOutcomeHeader = (title) => {
    // console.log('-  outcome:', title)
    // console.log('## outcome:', title.match(outcomeTitlePattern))
    if (outcomeTitlePattern.test(title)) {
        let [_, owner, initial, expected, description] = title.match(outcomeTitlePattern)
        return {
            owner, initial, expected, description: description.trim()
        }
    } else {
        return {
            type: 'error',
            content: title,
            message: "🤷‍♂️ Cannot parse the outcomeHeader 🚨"
        }
    }
}
const handleOutcomeElements = (oel) => {
    let elements = oel.reduce((p, c) =>
        ['progress', 'tasks', 'comments']
            .includes(c.toLowerCase())
            ? { ...p, reading: c.toLowerCase() }
            : { ...p, result: { ...p.result, [p.reading]: c } }
        , { result: { progress: null, tasks: null, comments: null } }
    ).result

    if (elements.progress) {
        elements.progress = readCheckins(elements.progress)
    }
    if (elements.tasks) {
        elements.tasks = readTasks(elements.tasks)
    }
    if (elements.comments) {
        elements.comments = readComments(1)(elements.comments.trim())
    }

    return elements
}
const checkinPattern = /-? *([\d]+) *, *(\d{4}-\d{2}-\d{2}) *([\w| ]*) *:([\w|\W]*)/im
const readCheckins = (pro) => {
    let progress = pro.trim().split('\n    -').map(fg => {
        let [checkin, ...comments] = fg.split(commentRegexForLevel(2))
        let [_, progress, date, confidence, analysis] = checkinPattern.exec(checkin)
        return {
            type: 'checkin',
            // title: checkin,
            progress, date, confidence, analysis,
            comments: comments.map(readComments(2))
        }
    })
    return progress
}
const taskPattern = /-? *\[([ |x]?)\] *(?:(@[\w]+)|( ?)) +(?:(\d{4}-\d{2}-\d{2})|( ?)) *(?:(\d{4}-\d{2}-\d{2})|( ?)) *:? *([\w\W]*)/im
const readTasks = (tsk) => {
    let tasks = tsk.trim().split('\n    -').map(tx => {
        let [t, ...comments] = tx.split(commentRegexForLevel(2))
        let [_, done, owner, _2, dueAt, _3, completedAt, _4, description] = taskPattern.exec(t)
        // console.log('## task:', {done, owner, _2, dueAt, _3, completedAt, _4, description})
        // console.log('## task:', taskPattern.exec(t))
        return {
            type: 'task',
            // title: t,
            done: ['x', 'X'].includes(done)
            , owner, dueAt, completedAt,
            description: description.trim(),
            comments: comments.map(readComments(2))
        }
    })
    return tasks
}
const commentRegexForLevel = lvl => new RegExp('^' + ' '.repeat(lvl * 4) + '- >', 'igm')
const readComments = lvl => (comm) => {
    let nxpttrn = commentRegexForLevel(lvl + 1)
    if (nxpttrn.test(comm)) {
        let [h, ...rs] = comm.split(nxpttrn)
        return { type: 'comment', title: h, comments: rs.map(readComments(lvl + 1)) }
    } else {
        return { type: 'comment', title: comm }
    }
}
const outcomePattern = /- *(Progress|Tasks|Comments)/im
const handleOutcomes = (ot) => {
    return ot.map(out => {
        if (outcomePattern.test(out)) {
            let [header, ...rawElements] = out.split(outcomePattern)
            return {
                type: 'outcome',
                ...handleOutcomeHeader(header),
                ...handleOutcomeElements(rawElements),
            }
        } else {
            return {
                type: 'error',
                content: out,
                message: '🚨Cannot handle this outcome🚨',
                test: 'outcomePattern.test(out)',
                result: outcomePattern
            }
        }
    })
}

const parse = (origin) =>
    origin
        .trim()
        .split('\n# ')
        .map(plan => {
            if (plan.includes('\n## ')) {
                let [header, ...objectives] = plan.split('\n## ')
                return {
                    type: 'plan',
                    ...readTitleHeader(header),
                    objectives: handleObjectives(objectives)
                }
            } else {
                return {
                    type: 'error',
                    content: plan,
                    message: "🤷‍♂️ where are the objectives? 🚨"
                }
            }
        })
export { parse }