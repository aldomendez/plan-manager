import { ref } from 'vue'
let plans = ref([
  {
    "type": "plan",
    "title": "Plan title",
    "completeness": "0",
    "priority": "A",
    "begin": "2021-07-01",
    "ends": "2021-08-01",
    "description": [
      "Check-ins due on Monday"
    ],
    "objectives": [
      {
        "type": "objective",
        "title": "Objective",
        "outcomes": [
          {
            "type": "outcome",
            "owner": "@amendez",
            "initial": "0",
            "expected": "80%",
            "description": "Outcome description",
            "progress": [
              {
                "type": "checkin",
                "progress": "3",
                "date": "2021-07-06",
                "confidence": "at risk",
                "analysis": "How do I get here? \n    And multi line checkout lines\n",
                "comments": [
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comments\n"
                  },
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comments\n",
                    "comments": [
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comments\n"
                      },
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comments\n"
                      },
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comments"
                      }
                    ]
                  }
                ]
              },
              {
                "type": "checkin",
                "progress": "5",
                "date": "2021-07-08",
                "confidence": "on track",
                "analysis": " progress comment",
                "comments": []
              }
            ],
            "tasks": [
              {
                "type": "task",
                "done": false,
                "owner": "@mendez",
                "dueAt": "2021-07-06",
                "completedAt": "2021-07-14",
                "description": "tarea una muy importante que esta pendiente por hacer y con mucho texto para que sea bien importante y dificil de leer",
                "comments": []
              },
              {
                "type": "task",
                "done": true,
                "description": "Create list of top 10 legacy design issues",
                "comments": [
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comment\n",
                    "comments": [
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comment"
                      }
                    ]
                  }
                ]
              },
              {
                "type": "task",
                "done": false,
                "description": "Secure UX sprint with Product teams",
                "comments": []
              }
            ],
            "comments": {
              "type": "comment",
              "title": "- > @amendez 2021-07-06: Comments\n",
              "comments": [
                {
                  "type": "comment",
                  "title": " @amendez 2021-07-06: Comments\n",
                  "comments": [
                    {
                      "type": "comment",
                      "title": " @amendez 2021-07-06: Comments"
                    }
                  ]
                }
              ]
            }
          },
          {
            "type": "outcome",
            "owner": "@mcampos",
            "initial": "0",
            "expected": "80%",
            "description": "Outcome description",
            "progress": [
              {
                "type": "checkin",
                "progress": "3",
                "date": "2021-07-06",
                "confidence": "at risk",
                "analysis": " How do I get here? \n    And multi line \n    checkout lines\n",
                "comments": [
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comments"
                  }
                ]
              }
            ],
            "tasks": [
              {
                "type": "task",
                "done": true,
                "description": "Create list of top 10 legacy design issues",
                "comments": [
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comment"
                  }
                ]
              },
              {
                "type": "task",
                "done": false,
                "description": "Secure UX sprint with Product teams",
                "comments": []
              }
            ],
            "comments": {
              "type": "comment",
              "title": "- > @amendez 2021-07-06: Comments"
            }
          }
        ]
      },
      {
        "type": "objective",
        "title": "Objective2",
        "outcomes": [
          {
            "type": "outcome",
            "owner": "@amendez",
            "initial": "0",
            "expected": "80%",
            "description": "Outcome description",
            "progress": [
              {
                "type": "checkin",
                "progress": "5",
                "date": "2021-07-08",
                "confidence": "on track",
                "analysis": " progress comment",
                "comments": []
              },
              {
                "type": "checkin",
                "progress": "3",
                "date": "2021-07-06",
                "confidence": "at risk",
                "analysis": "How do I get here? And multi line checkout lines\n",
                "comments": [
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comments\n"
                  },
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comments\n",
                    "comments": [
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comments\n"
                      },
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comments"
                      }
                    ]
                  }
                ]
              }
            ],
            "tasks": null,
            "comments": null
          }
        ]
      }
    ]
  },
  {
    "type": "plan",
    "title": "Plan title number 2",
    "completeness": "0",
    "priority": "A",
    "begin": "2021-07-01",
    "ends": "2021-08-01",
    "description": [
      "Check-ins due on Monday",
      "- > @amendez 2021-07-06: Not sure if this can have comments",
      "    - > @amendez 2021-07-06: But sure comments can have comments in them"
    ],
    "objectives": [
      {
        "type": "objective",
        "title": "Objective2",
        "outcomes": [
          {
            "type": "outcome",
            "owner": "@amendez",
            "initial": "0",
            "expected": "80%",
            "description": "Outcome description",
            "progress": [
              {
                "type": "checkin",
                "progress": "5",
                "date": "2021-07-08",
                "confidence": "on track",
                "analysis": " progress comment",
                "comments": []
              },
              {
                "type": "checkin",
                "progress": "3",
                "date": "2021-07-06",
                "confidence": "at risk",
                "analysis": "How do I get here? And multi line checkout lines\n",
                "comments": [
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comments\n"
                  },
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comments\n",
                    "comments": [
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comments\n"
                      },
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comments"
                      }
                    ]
                  }
                ]
              }
            ],
            "tasks": [
              {
                "type": "task",
                "done": false,
                "owner": "@mendez",
                "dueAt": "2021-07-06",
                "description": "tarea una muy importante que esta pendiente por hacer y con mucho texto para que sea bien importante y dificil de leer",
                "comments": []
              },
              {
                "type": "task",
                "done": false,
                "description": "Create list of top 10 legacy design issues",
                "comments": [
                  {
                    "type": "comment",
                    "title": " @amendez 2021-07-06: Comment\n",
                    "comments": [
                      {
                        "type": "comment",
                        "title": " @amendez 2021-07-06: Comment"
                      }
                    ]
                  }
                ]
              },
              {
                "type": "task",
                "done": false,
                "description": "Secure UX sprint with Product teams",
                "comments": []
              }
            ],
            "comments": {
              "type": "comment",
              "title": "- > @amendez 2021-07-06: Comments"
            }
          }
        ]
      }
    ]
  }
]
)


export { plans }