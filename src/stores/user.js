import { reactive, ref } from "vue";
import { get, post, setToken } from "../libs/fetch";
import axios from 'axios'


axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (error.response.status == 401) {
    state.user = null
  }
  return Promise.reject(error);
});

const state = reactive({
  user: null,
  selectedCompany: null,
  companies: {},
  loginState: "initial",
  message: null,
});

const initial = ref(true)

const initialUserCheck = async () => {
  return get("api/user").then((d) => {
    console.log('in then');
    state.user = d.data.user
    state.message = d.data.message
    initial.value = false
  }).catch(e => {}).finally(d => {
    initial.value = false

  })
}
initialUserCheck()

const logout = () =>
  post("api/logout")
    .then((d) => {
      state.user = null
      state.message = null
      setToken(null)
    }).catch(e => {})

const sendPasswordRecoveryEmail = (email) => {
  return post('api/send_password_recovery_email', {email})
}

const login = ({ email, password }) => {
  let device_name = 'nova-pix-techpack'
  return post("api/login", { email, password, device_name })
    .then((d) => {
      if (d.data.message) {
      } else {
        setToken(d.data)
        initialUserCheck()
      }
    }).catch(e => {});
}

export { state, logout, login, initial, sendPasswordRecoveryEmail, };
