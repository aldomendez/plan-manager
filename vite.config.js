import vue from '@vitejs/plugin-vue'

/**
 * @type {import('vite').UserConfig}
 */
export default {
  plugins: [vue()],
  base:'./',
  build:{
    outDir:'primary',
    manifest: true,
  }
}
