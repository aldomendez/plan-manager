
# Plan title
0% (A) 2021-07-01 2021-08-01 +tag @context key:value
Check-ins due on Monday
## Objective
### Outcome
- Progress
    - 3%, 2021-07-06 at risk
        How do I get here?
        And multi line checkout lines
        - > @amendez 2021-07-06: Comments
- Tasks
    - [ ] @owner Create list of top 10 legacy design issues
        - > @amendez 2021-07-06: Comment
    - [ ] Secure UX sprint with Product teams
- Comments
    - > @amendez 2021-07-06: Comments

# Clave para codificar la informacion de la aplicacion

## Team

Is a markdown table with handle, name, email, and role
The role can have any of: Admin, User, Owner

```
| handle     | name             | email                    | role  |
|------------|------------------|--------------------------|------:|
| @amendez   | Aldo, Mendez     | amendez@novalinkmx.com   | Admin |
| @jguerrero | Javier, Guerrero | jguerrero@novalinkmx.com | User  |
| @mcampos   | Manuel, Campos   | mcampos@novalinkmx.com   | Owner |
```


## Plan
This tag begins with a `#` Denote the title of the plan

```
┌───────────────────────────────────────▶Completion percent auto calculated on open
│   ┌───────────────────────────────────▶Marks priority
│   │       ┌───────────────────────────▶Begin date
│   │       │          ┌────────────────▶End date
│   │       │          │          ┌─────▶Description; tags (optional) can be placed anywhere in here 
│   │       │          │     ─────┴──────────────────────────────────────── 

0% (A) 2021-07-01 2021-08-01 descriptive text +tag @user #context key:value 

                                                │   │       │          │
                                                │   │       │          │
                                                │   │       │          └─▶special key/value tag
                                                │   │       └────────────▶Context tag
                                                │   └────────────────────▶User tag
                                                └────────────────────────▶Project tag
```
## Objective
Begins with `##` Denotes the objective just the title

## Outcome
The outcome is the most complex element in this project, has progress and task assigned, and comments to
each of the elements

### Outcome header
Begin with `###` then the text, the user and the target initial and expected value including the format

Example `### 80% of UI Components can be found in a design system @mendez 0% to 80%`
```
### 80% of UI Components can be found in a design system @mendez 0% to 80%                                
 │  ─────┬──────────────────────────────────────────────    │    │     │                                  
 │       │                                                  │    │     └─▶Expected value including format 
 │       │                                                  │    └───────▶Initial value                   
 │       │                                                  └────────────▶Owner                           
 │       └───────────────────────────────────────────────────────────────▶Description                     
 └───────────────────────────────────────────────────────────────────────▶Begin tag
 ```
### Outcome elements
Can have `Progress`, `Tasks`, `Comments` with the format of a first level md list

#### Progress
Progress have just second level md list elements in the form, example

```
      ┌──────────────────▶Check in value, including format     
      │        ┌─────────▶Check in date                        
      │        │      ┌──▶Any of: on track, at risk, off track 
      │        │      │                                        
    - 3%, 2021-07-06 at risk                                   
        How do I get here? │                                   
        And multi line     ├─────▶Multiline comment
                           │                                   
        - > @amendez 2021-07-06: Comments                      
               │          │          │                         
               │          │          └───▶Comments             
               │          └──────────────▶Comment date         
               └─────────────────────────▶Comment author       
```

#### Tasks
Tasks can have two types
- Simple
    - Just status and description
- Detailed, have
    - Status
    - optional owner
    - optional scheduled date
    - a colon to indicate where the task begins
    - task description
```
        ┌─────────────────────────▶Task completion                      
        │    ┌────────────────────▶Task owner                           
        │    │        ┌───────────▶Task due date
        │    │        │        ┌──▶Task description                     
- Tasks │    │        │        │                                        
    - [ ] @mendez 2021-07-06: tarea                                     
        - > @amendez 2021-07-06: Comment ──────▶Task comment            
    - [ ] Secure UX sprint with Product teams                           
       │          │                                                     
       │          └──────────────▶Task                                  
       └─────────────────────────▶Task completion                       
```

#### Comments
Comments anywhere they appear should have an arrow `- >`, author, and date of creation, before the comment
```
- > @amendez 2021-07-06: Comments               
        │          │          │                  
        │          │          └───▶Comments      
        │          └──────────────▶Comment date  
        └─────────────────────────▶Comment author
```