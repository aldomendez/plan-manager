# Plan title
> 0% (A) 2021-07-01 2021-08-01 +tag @context key:value along with a lengthy text description is a long set of things
Check-ins:Monday
## Objective
### Outcome description
> @amendez 0 to 80%: 
- Progress
    - 5, 2021-07-08 on track: progress comment
    - 3, 2021-07-06 at risk:How do I get here? And multi line checkout lines
        - > @amendez 2021-07-06: Comments
            - > @amendez 2021-07-06: Comments
            - > @amendez 2021-07-06: Comments
            - > @amendez 2021-07-06: Comments
            - > @amendez 2021-07-06: Comments
            - > @amendez 2021-07-06: Comments
- Tasks
    - [ ] @mendez 2021-07-06: tarea una muy importante que esta pendiente por hacer y con mucho texto para que sea bien importante y dificil de leer
    - [ ] Create list of top 10 legacy design issues
        - > @amendez 2021-07-06: Comment
            - > @amendez 2021-07-06: Comment
            - > @amendez 2021-07-06: Comment
            - > @amendez 2021-07-06: Comment
            - > @amendez 2021-07-06: Comment
    - [ ] Secure UX sprint with Product teams
- Comments
    - > @amendez 2021-07-06: Comments
### @mcampos 0 to 80%: Outcome description
- Progress
    - 3, 2021-07-06 at risk: How do I get here? And multi line checkout lines
        - > @amendez 2021-07-06: Comments
- Tasks
    - [ ] Create list of top 10 legacy design issues
        - > @amendez 2021-07-06: Comment
    - [ ] Secure UX sprint with Product teams
- Comments
    - > @amendez 2021-07-06: Comments

# Team

| handle     | name             | email                    | role  |
|------------|------------------|--------------------------|------:|
| @amendez   | Aldo, Mendez     | amendez@novalinkmx.com   | Admin |
| @jguerrero | Javier, Guerrero | jguerrero@novalinkmx.com | User  |
| @mcampos   | Manuel, Campos   | mcampos@novalinkmx.com   | Owner |
| @mcampos   | Manuel, Campos   | mcampos@novalinkmx.com   | manager |
| @mcampos   | Manuel, Campos   | mcampos@novalinkmx.com   | contributor |